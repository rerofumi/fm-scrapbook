---
title: mastodon
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# mastodon

分散型マイクロブログSNS。

各サーバーはインスタンスと呼ばれ、各インスタンスはユーザー情報や記事をやりとりしつつ相互運用ができる。

[www.mofgao.space](https://www.mofgao.space/) を運用中。その運用に関したメモなど。

# 記述日

- Dec.09.2017 - カテゴリページ作成

# mastodon のリポジトリ

[https://github.com/tootsuite/mastodon](https://github.com/tootsuite/mastodon)

# pages


[Mastodon を Docker で運用するときに IPv6 NAT を適用する方法](docker_ipv6.md)
