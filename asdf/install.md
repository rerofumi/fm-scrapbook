---
title: asdf/install
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[asdf top へ戻る](asdf.md)

asdf のインストールメモ。


# 記述日

- Dec.02.2017 - トップとインストールを分離
- Nov.14.2017 - 初出

# 記述時の前提

- asdf - 0.4.0

# インストール

[https://github.com/asdf-vm/asdf](https://github.com/asdf-vm/asdf)

## 対象

 - Debian9, Ubuntu 16.10
 - Windows Linux Subsystem with Ubuntu
 - macOS with Homebrew

## Debian インストール手順

あらかじめインストールしておくパッケージ

```
$ sudo apt install build-essential automake autoconf libreadline-dev libncurses-dev libssl-dev libyaml-dev libxslt-dev libffi-dev libtool unixodbc-dev
```

リポジトリのクローン

```
$ git clone https://github.com/asdf-vm/asdf.git ~/.asdf
$ cd ~/.asdf
$ git checkout -b v0.4.0 refs/tags/v0.4.0
```

asdf が実行できるよう環境変数等の設定

```
$ echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bashrc
$ echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ~/.bashrc
```

# プラグイン

asdf で管理するソフトウェアはプラグインを追加することで利用できる。
各プラグインは別途リポジトリとして提供されていて、自分で必要なものをチョイスしてインストールする。

## プラグインリスト

[https://github.com/asdf-vm/asdf-plugins](https://github.com/asdf-vm/asdf-plugins)

## プラグインの追加

```
$ asdf plugin-add (ソフトウェアプレフィクス)　(プラグインリポジトリ)

$ asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang
$ asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir
```

0.4.0 では上記プラグインリストの URL が既にしこんであり git リポジトリを指定しなくても良いみたい。

```
$ asdf plugin-add erlang
$ asdf plugin-add elixir
```

## プラグインの更新

```
$ asdf plugin-update --all
```

## インストール可能なソフトウェアバージョンリストの表示

```
$ asdf list-all (ソフトウェアプレフィクス)

$ asdf list-all erlang
```

## ソフトウェアのインストール

```
$ asdf install (ソフトウェアプレフィクス)　(ソフトウェアバージョン)

$ asdf install erlang 20.1
$ asdf install elixir 1.5.2
```

## ソフトウェアのバージョン切り替え

ソフトウェアはインストールしただけでは利用可能にならない、使うバージョンを指定する必要がある。

複数バージョンを入れておいて、必要に応じて使うバージョンを切り替えることができる。

```
$ asdf global (ソフトウェアプレフィクス) (ソフトウェアバージョン)

$ asdf global erlang.20.1
$ asdf global elixir 1.5.2
```

# プラグインメモ

## golang pagenation対応

一時期 golang plugin は 1.9.0 以降がインストールできない不具合があったが現在は修正がマージされた。(Dec.02.2017)

