---
title: asdf/emacs-gomode
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[asdf top へ戻る](asdf.md)

Emacs で go言語開発環境を整えようとした場合 asdf の path が通らなくなる問題がある。

それを回避しつつ環境を整えるメモ。

# 記述日

- Dec.02.2017 - メモ追加

# 前提

- Emacs 24
- go 1.9.2
- asdf 0.4.0

# 問題点

asdf はユーザー環境での bashrc に追加する形でシステムの PATH よりも asdf やインストールされた
環境を優先して実行する形を取る。これは nvm や rdenv といったツールと同じ機構である。

この場合ユーザーのシェル上では PATH 等の環境が正しく整えられて問題が生じないのだが、
Emacs 等をデスクトップメニューから実行した場合 bashrc を経由していないため asdf が用意した環境が
一切利用できないという問題を持っている。

go言語の場合、gofmt, godoc, godef, golint といったサポートツールをたくさん用意してそれらを組み合わせて
環境をつくるといったエコシステムを持っている。これらサポートツールへの PATH が正しく通っている事が
前提となる。
ところが、Emacs では先ほど書いたようにユーザーログインシェルでの環境変数を全て持っているとは
限らないため、これらサポートツールへアクセスできない状況を持ち得る。

## 解決策

emacs の初期化時に asdf や gopath へのパスを通してやる。


# go サポートツール

あらかじめインストールしておく。

```
$ go get -u github.com/rogpeppe/godef
$ go get -u github.com/nsf/gocode
$ go get -u github.com/golang/lint/golint
$ go get -u github.com/kisielk/errcheck
```

# emacs 設定

## パッケージのインストール

- flycheck - 構文チェックをしてくれるエンジン
- company-go - 補完エンジン

## emacs 設定

.emacs とか .emacs.d/init.el とか、.emacs.d/ の下とか。

`"~/develop/go"` はうちの環境での `$GOPATH` なので手元では適当に編集して使用すること。

```lisp:.emacs
;;;
;;; golang devenv
;;;
(setenv "GOPATH" (expand-file-name "~/develop/go"))
(setenv "PATH" (concat
                (expand-file-name "~/.asdf/shims") ":"
                (expand-file-name "~/develop/go/bin") ":"
                (getenv "PATH")))
(setenv "GOROOT" (replace-regexp-in-string "[ \t\n]*$" "" (shell-command-to-string "go env GOROOT")))
(add-to-list 'exec-path (expand-file-name "~/.asdf/shims"))
(add-to-list 'exec-path (expand-file-name "~/develop/go/bin"))
;
(add-hook 'go-mode-hook 'company-mode)
(add-hook 'go-mode-hook 'flycheck-mode)
(add-hook 'go-mode-hook (lambda()
           (add-hook 'before-save-hook' 'gofmt-before-save)
           (local-set-key (kbd "M-.") 'godef-jump)
           (set (make-local-variable 'company-backends) '(company-go))
           (company-mode)
           ))
;;(require 'go-eldoc)
;;(add-hook 'go-mode-hook 'go-eldoc-setup)
```

## 効能

- `M-x godoc` でのドキュメント参照
  - これができたら必要なパスが通っているという確認になる
- `M-x gofmt` バッファのソースを整形
  - 上記設定ではセーブ前に自動的に gofmt を実行している
- メソッド名補完ができるようになる
- 文法チェックで編集中にエラー箇所がわかる
- `M-.` でメソッドや変数の定義箇所にジャンプできる

## 今回は使っていない機能、パッケージ

- go-eldoc
  - 編集中カーソルが当たっているメソッドのヘルプがミニバッファに表示されるパッケージ


# あとがき

なんか上手くいかなくて試行錯誤していた結果なので余計なパス設定がくっついているかもしれない。


