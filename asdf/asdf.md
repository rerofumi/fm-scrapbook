---
title: asdf
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# asdf

プログラミング言語を中心としたパッケージインストーラ。

nvm や rdenv の様にユーザーの home 領域にパッケージをインストールする形で /usr 等のシステム権限が必要な領域を使用しないのが特徴。

複数のバージョンを入れて、使用するバージョンを切り替えられるので開発向き。

Elixir と erlang の最新版インストール方法を探していてたどり着いた。

# 記述日

- Dec.02.2017 - インストール手順をページ分け
- Nov.14.2017 - 初出

# asdf のリポジトリ

[https://github.com/asdf-vm/asdf](https://github.com/asdf-vm/asdf)

# pages

[asdf インストールメモ](install.md)

[asdf 環境下での Emacs golang 開発環境を整える手順](emacs-gomode.md)


