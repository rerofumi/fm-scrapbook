---
title: kubernetes
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# kubernetes

docker コンテナのオーケストレーションシステム。

複数のサーバーを node として束ね、そのうえで pod と呼ばれる単位でコンテナを実行、動作を管理することができるシステム。

docker のオーケストレーションは複数あったが、2018 年は kubernetes がデファクトスタンダードになりつつある。

# 記述日

-   Oct.14.2018 - カテゴリページ作成、インストールメモ作成

# kubernetes の公式サイト

[https://kubernetes.io/](https://kubernetes.io/)

# pages

[kubernetes のインストール手順](install.md)

[コンテナ操作の作業メモ](container_work.md)

[イングレスを使う](nginx_ingress.md)

[小ネタメモ](memorandom.md)
