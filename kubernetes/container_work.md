---
title: kubernetes/container_work
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[kubernetes top へ戻る](kubernetes.md)

kubernetes 上でコンテナを動作させる手順のメモ

# 記述日

-   Oct.21.2018 - 記述開始

# 前提環境

-   kubernetes - v1.12
-   インストール先 OS - Debian9.5

# 動作確認

## pod の起動

正確には deployment の起動。

```
kubectl run (name) --image=(docker image)
```

## pod へのアクセス

```
kubectl exec -it (pod name) (command)
```

## deployment の確認

```
kubectl get deployment.apps
```

## pod deployment の削除

```
kubectl delete deployment.apps (deployment)
```

## host への port 接続

Ingress よりも簡素な接続。

アクセス先は master か node のどこか。port も動的にどこか。

```
kubectl expose deployment (deployment name) --type=LoadBalancer
```

サービスとして起動される。

```
kubectl get service
```

サービスとして削除。

```
kubectl delete service (name)
```

固定ポート番号で pod 1 つに接続。target がコンテナの port。

```
kubectl expose deployment (name) --port=8888 --target-port=80
```

# プライベートレジストリの使用

## プライベートレジストリの登録

レジストリアクセスクレデンシャルの登録。

ネームスペース毎。

```
kubectl create secret docker-registry (registory name) --docker-server=(url) --docker-username=(user) --docker-password=(passwd) --docker-email=(email) --namespace=(name)
```

## プライベートレジストリからのイメージ起動

`imagePullSecrets` を指定する

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: myapp
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: myapp
    spec:
      containers:
      - name: myapp
        image: (プライベートリポジトリを含んだイメージ名)
        ports:
        - containerPort: (port)
      imagePullSecrets:
      - name: (secret で登録したレジストリ名)
```

# データベースとそこにリンクするコンテナの作成

## 1. redis DB の起動

```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: mary-redis
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: mary-station
        role: redis-db
    spec:
      containers:
      - name: mary-redis
        image: redis
        ports:
        - containerPort: 6379
```

## 2. DB へのサービス接続を作成

```
apiVersion: v1
kind: Service
metadata:
  name: mary-redis
spec:
  ports:
  - port: 6379
    targetPort: 6379
  selector:
    app: mary-station
    role: redis-db
```

## 3. DB に接続して利用するコンテナ

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mary-station
  labels:
    app: mary-station
spec:
  replicas: 1
  selector:
    matchLabels:
      app: mary-station
  template:
    metadata:
      labels:
        app: mary-station
        role: bot
    spec:
      containers:
      - name: mary-station
        image: fumi2kick.azurecr.io/mary_station
        env:
        - name: REDIS_HOST
          value: "mary-redis"
        - name: REDIS_PORT
          value: "6379"
      imagePullSecrets:
      - name: fumi2kick-cr
```
