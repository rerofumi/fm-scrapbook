---
title: kubernetes/nginx_ingress
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[kubernetes top へ戻る](kubernetes.md)

kubernetes で NGINX Ingress を利用する。

# 記述日

-   Oct.27.2018 - 記述開始

# 前提環境

-   kubernetes - v1.12
-   インストール先 OS - Debian9.5

# Ingress とは

-   Kubernetes のサービスを外部に公開するためのアプリケーションロードバランサ
-   URL やパスを見て公開する Kubernetes 内のサービス(pod)に割り振っていく
-   標準のサービスではホスト上の任意のポートに公開されるが、それをそのままインターネットに公開するわけにもいかないし、アプリケーションロードバランサを用意するにしてもサービスのポートが起動毎に変わるしで面倒だった
-   Ingress 自体は標準でも使えるが、動作はしない
-   Ingress controller という実際にロードバランサ部分を動作させる機構が必要
    -   Ingress controller はクラウド上であったらクラウドロードバランサを自動設定したりする
    -   自前で設定したり、オンプレミスに構成した Kubernetes では Ingress controller が存在していない
    -   そういった自前 Kubernetes システムでは NGINX-Ingress-controller を利用することで Ingress が使えるようになる

## 自分のところでのユースケース

-   元々インターネットに公開するためのリバースプロキシを用意していて、そこから Kubernetes へと割り振っていたけれども、いちいちサービスのポートを調べて URL に設定していくのが面倒になったのでそこを Kubernetes 上で済ませたかった
-   Ingress の上にもう一段あるリバースプロキシで、wild card URL を受けてそれを全て後段の Ingress に回すようにした
-   Ingress で実際に URL のマッチを行って、必要なサービスに割りふる
-   この構成によって、サービスが増えて URL を用意したとしても Kubernetes の設定上で行えるようになった
    -   リバースプロキシ部分の設定はいじらなくても良くなった

# NGINX Ingress を使う

## インストール

-   Helm でインストールするのが楽
    -   Helm は kubernetes 用のパッケージマネージャ
    -   パッケージマネージャ形式で yaml をダウンロード、展開しアプリを利用することができる
    -   インストールガイド (https://docs.helm.sh/using_helm/#quickstart-guide)

```
$ helm install --name nginx-ingress stable/nginx-ingress --namespace (展開するネームスペース)
```

helm でインストールすると `nginx-ingress-default-backend` と `nginx-ingress-controller` の 2 つの pod が起動する。前者はアクセス URL が無かったときの 404 を返すためのダミー HTTP サービスで、後者が L7 ロードバランサー。

helm から起動した ingress-controller は service で見ると LoadBalancer として登録されており、ホストには任意の児童割り当てポートが割り振られている。要注意ポイント。
前段に proxy がある場合はこのポートに対してフォワードする。

ポートを 80,443 にするには helm に因らず自分で yaml を書いて起動すればよさそうだが、今回はそこまでやる必要は無かったので未確認。

## 使い方

### サービスディスカバリの作成

イングレスがサービス(pods)を見つけるための名前設定を行う。

pod のセレクタと、アクセスポート、サービス名を定義。`NodePort` type で作成。

```
apiVersion: v1
kind: Service
metadata:
  name: (サービス名)
  namespace: (名前空間)
spec:
  type: NodePort
  ports:
  - port: (コンテナのポート)
    protocol: TCP
    targetPort: (コンテナのポート)
  selector:
    (pod セレクトタグ)
```

`kubectl get service` でできたことを確認。

### イングレスの作成

http を受けてサービスへ繋ぐイングレスの作成。
L7 ロードバランサなので、受け取る url やパスによってバックグラウンドの投げ先をわけることができる。

```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: (イングレス名)
  namespace: (名前空間)
spec:
  rules:
  - host: (受け取る URL)
    http:
      paths:
      - path: /(URL のパス)
        backend:
          serviceName: (サービス名)
          servicePort: (サービスのポート)
```

`kubectl get ingresses.extensions` でできたことを確認。

## おまけ

### helm install でエラーが出たとき

`Error: release nginx-ingress failed: namespaces "mofgao" is forbidden: User "system:serviceaccount:kube-system:default" cannot get resource "namespaces" in API group "" in the namespace "mofgao"`

helm がインストールする tiller のロール(権限)が足りない。

Teller service を削除して tiller アカウントを作成。

`kubectl delete deployments.apps --namespace=kube-system tiller-deploy`

```
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
clusterrolebinding.rbac.authorization.k8s.io/tiller-cluster-rule created
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```

`helm init --upgrade`
