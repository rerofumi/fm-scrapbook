---
title: kubernetes/memorandom
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[kubernetes top へ戻る](kubernetes.md)

kubernetes 関連の小ネタ

# 記述日

-   Oct.27.2018 - 記述開始

# 前提環境

-   kubernetes - v1.12
-   インストール先 OS - Debian9.5

# Memo

-   複数の作業 YAML をまとめて 1 つの YAML にしたい

    -   `---` で区切って連結するとまとめて処理される

-   pod を強制停止する
    -   `--force --grace-period=0` をつけて削除
    -   `kubectl --namespace=mofgao delete pods --force --grace-period=0 (pod name)`
