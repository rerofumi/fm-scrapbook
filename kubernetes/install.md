---
title: kubernetes/install
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[kubernetes top へ戻る](kubernetes.md)

kubernetes のインストール作業メモ

# 記述日

-   Oct.21.2018 - 一通り動作する物ができたので最初のリリース
-   Oct.14.2018 - インストールしながらメモを残す

# 前提環境

-   kubernetes - v1.12
-   インストール先 OS - Debian9.5

# インストール

## 事前準備

swap の停止、`fstab` も編集しておく。

```
swapoff -a
```

apt パッケージで入れられるバージョンの確認
sudo apt-cache showpkg パッケージ名

Docker のバージョンを下げる。(必要な場合)

```
apt purge docker-ce
apt autoremove
apt install docker-ce=17.03.3~ce-0~debian-stretch
apt-mark hold docker-ce
```

カーネルモジュールの追加。

`/etc/modules-load.d/ip_vs.conf`

```
ip_vs
ip_vs_rr
ip_vs_wrr
ip_vs_sh
```

docker host の cgroup を systemd に変更する。

以下のオプションを docker daemon の起動スイッチにつける。

```
--exec-opt native.cgroupdriver=systemd
```

## kubeadm のインストール

master, node の両方で。

```
apt-get update && apt-get install -y apt-transport-https curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
```

master 側で kubectl の設定。`.bashrc` に追加。

```
apt install bash-completion
```

```
export KUBECONFIG=/etc/kubernetes/admin.conf
source <(kubectl completion bash)
```

## master

ネットワークコントローラーに Flannel を使う場合 cidr を 10.244.0.0/16 に設定する。

token TTL はお好みで。

```
kubeadm config images pull
kubeadm init --pod-network-cidr=10.244.0.0/16 --token-ttl 0 --apiserver-advertise-address=10.0.5.3
```

ネットワークコントローラーの適用。

```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml
```

システムポッドの確認。

```
kubectl get pods --all-namespaces
```

マスターアイソレーション

```
kubectl taint nodes --all node-role.kubernetes.io/master-
```

ノードの確認。

```
kubectl get nodes -o wide
```

join コマンドの再発行。

```
kubeadm token create --print-join-command
```

## node

master の `kubeadm init` したときに表示された join コマンドを入力する。

忘れたり、トークン期限切れしてたりした場合は `kubeadm token create` で作り直す。

```
kubeadm join 10.0.5.3:6443 --token (トークン) --discovery-token-ca-cert-hash sha256:(ハッシュ)
```

node roles label

```
kubectl label node (ノード名) node-role.kubernetes.io/worker=worker
```

## client

`/etc/kubernetes/admin.conf` を home の .kube に置く。

これでリモート越しに kubectl を使えるようになる。

```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
