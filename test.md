---
title: markdown page
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---



test file
=====

# hoge
text

## fuga

- aaaa
  - bbbb
  - vvvv

### moga

| aa | bb | cc | dd |
|----|----|----|----|
|ee|ff|gg|hh|

# foo

`bar`

	block
	source
	indent


```ruby
require 'hode'

main()
```
