---
title: peertube
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# peertube

WebTorrent や ActivityPub を使った分散型動画投稿＆配信システム。

ActivityPub 対応なので、Mastodon等からユーザーをフォローして投稿情報を TL に受け取ったり、
動画記事に返信することでコメントをつけたりすることができる。

[peertube.mofgao.space](https://peertube.mofgao.space/) を運用中。その運用に関したメモなど。

# 記述日

- Mar.31.2018 - カテゴリページ作成

# mastodon のリポジトリ

[https://github.com/Chocobozzz/PeerTube](https://github.com/Chocobozzz/PeerTube)

# pages

[PeerTube のインストール手順](install.md)

[PeerTube でできることメモ](memo.md)

