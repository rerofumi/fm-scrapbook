---
title: peertube/install
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[peertube top へ戻る](peertube.md)

peertube のインストールメモ

# 記述日

- Mar.31.2018 - peertube のインストール手順書として記述

# 前提環境

- peertube - v1.0.0-beta3
- インストール先 OS - Debian9.4

# インストール

## 事前準備

```shell
apt update
apt upgrade
apt install nano less wget curl git
apt install build-essential
apt install gnupg2
```

## node.js のインストール

apt リポジトリの追加とインストール

```shell
wget -O- https://deb.nodesource.com/setup_8.x | bash
apt install nodejs
```

## yarn のインストール

apt リポジトリの追加とインストール

```shell
wget -O- https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt update
apt install yarn
```

## PeerTube 周辺パッケージ

```shell
apt install ffmpeg postgresql postgresql-client openssl redis-server
```

## PeerTube のビルド

```shell
git clone https://github.com/Chocobozzz/PeerTube
cd PeerTube
```

任意のバージョンを使用します
```shell
git checkout -b v1.0.0-beta3 v1.0.0-beta3
```

パッケージのインストールとビルド
```shell
yarn install
npm run build
```

## PeerTube コンフィグファイルの用意

```shell
cp config/production.yaml.example config/production.yaml
```

production.yaml を適切に編集して設定します。

## データベースの作成

`peertube` というユーザーと `peertube_prod` というデータベースを作成。
パスワードはコンフィグファイルの中に設定したものと同じものに設定。

```shell
(rootから)
su - postgres
createuser -P peertube
(コンソールにパスワード入力を求められるので設定)
createdb -O peertube peertube_prod
```

# 起動

ここまでで準備が整っているはずなので起動。

```shell
NODE_ENV=production npm start
```

初回起動のコンソールログに root のパスワードが表示されているのでメモする。

もし見逃してしまったときは DB を削除してやり直すしかない。

## systemd ユニットファイル

記述例

```systemd
[Unit]
Description=PeerTube daemon
After=network.target postgresql.service

[Service]
Type=simple
Environment=NODE_ENV=production
Environment=NODE_CONFIG_DIR=/root/PeerTube/config
#User=peertube
#Group=peertube
ExecStart=/usr/bin/npm start
WorkingDirectory=/root/PeerTube
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=peertube
Restart=always

[Install]
WantedBy=multi-user.target
```

`/root` に置いてそのまま起動しているけれども、本当は適切な位置に設置して peertube アカウントで起動するのが正解。

# 動画

インストール実況動画

[https://peertube.mofgao.space/videos/watch/8e0dbc9c-bfa0-46ae-84cc-22a0cbd249e5](https://peertube.mofgao.space/videos/watch/8e0dbc9c-bfa0-46ae-84cc-22a0cbd249e5)