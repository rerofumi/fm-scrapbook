---
title: pleroma
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# pleroma

ActivityPub 互換で GNU Social や Mastodon とつながれる SNS 実装。
UI 的な見た目は GNU Social クローン的。

Mastodon とやりとりできるけれども GNU Social より新しく、軽量なあたりが注目ポイント。

# 記述日

- Nov.19.2017 - ページ分け＆RasPiインストール検討を追加
- Nov.18.2017 - 設定ファイルと dockerlize について更新
- Nov.15.2017 - メモ書きとして記す

# pleroma のリポジトリ

[https://git.pleroma.social/pleroma/pleroma](https://git.pleroma.social/pleroma/pleroma)

# pages

[pleroma インストールメモ](install.md)

[pleroma Dockerlize のヒント](dockerlize.md)

[pleroma RaspberryPi へのインストール手順](raspberrypi.md)
