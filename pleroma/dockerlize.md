---
title: pleroma/dockerlize
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[preloma top へ戻る](pleroma.md)

pleroma を Docker container で動作させるときのヒント

# 記述日

- Nov.19.2017 - pleroma の dockerlize メモとして分割


# Dockerlize を考える

docker コンテナでのテストや運用をするために。

## 参考 Dockerfile 

pleroma リポジトリは git clone して Dockerfile と同じ階層に置いておくものとする。

```
FROM elixir:alpine

RUN apk --update add git make

ADD pleroma /root/pleroma/
ADD prod.secret.exs /root/pleroma/config
ADD terms-of-service.html /root/pleroma/priv/static/static/
ADD config.json /root/pleroma/priv/static/static/
ADD mofgao_bg.jpg /root/pleroma/priv/static/static/
ADD mofgao_icon.png /root/pleroma/priv/static/static/
ADD start.sh /root
RUN chmod 755 /root/start.sh

RUN mkdir /uploads
RUN rm -rf /root/pleroma/uploads
RUN ln -s /uploads /root/pleroma/uploads

WORKDIR /root/pleroma
ENV MIX_ENV prod
RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix clean
RUN mix deps.get
RUN mix deps.compile
RUN mix compile
RUN mix phx.digest

EXPOSE 4000
CMD ["/root/start.sh"]
```

`prod.secret.exs` は自分のサイト設定を仕込んだ上で用意しておく。

その他 `/root/pleroma/priv/static/static/` にコピーしているファイルはカスタマイズの為なので取り敢えず動かしたい場合は削っておく。
カスタマイズしたい場合はこれらファイル名を参考に用意する。

`uploads` はアップロードした画像ファイル等がストックされるディレクトリ。
このコンテナの中で永続化が必要な箇所なので `/uploads` とわかりやすい所に置いてシンボリックリンクで利用している。

### 起動スクリプト

上記 Dockerfile 内で扱っている start.sh スクリプト。
DB がなかったら作成し起動している。

```
#!/bin/sh

sleep 5

cd /root/pleroma
MIX_ENV=prod
export MIX_ENV
mix ecto.create
mix ecto.migrate
mix phx.server
```


## docker-compose ファイル

```
version: "3"
services:
  db:
    image: postgres:alpine
    environment:
      - POSTGRES_PASSWORD=postgres
      - POSTGRES_USER=postgres
    ports:
      - 5432:5432

  pleroma:
    image: pleroma
    build: .
    ports:
      - 4000:4000
```

postgres の port forword は無くても良いが DB コンテナだけ立ち上げてデバッグ確認したいときがあるので記述してある。

上記例だと `db` が postgres の host 名になるので、`prod.secret.exs` 等で指定する DB host を `db` にしておく。


## 永続化を考える

今のところ永続化するのに必要なストレージは

  - PostgreSQL の DB ファイル
  - Pleroma の uploads ディレクトリ

の 2つ。

postgres は `/var/lib/postgresql/data` をローカルボリュームにマウントして永続化。

pleroma は上記 Dockerfile だと `/uploads` をローカルボリュームにマウントして永続化。

これでいけるはず。
