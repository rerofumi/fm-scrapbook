---
title: pleroma/raspberrypi
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[preloma top へ戻る](pleroma.md)

pleroma を RaspberryPi にインストールする手順のメモ

# 記述日

- Nov.19.2017 - メモ書き開始

# 前提

- RapberryPi2
- Noobs 2.4.4 (Release date 2017-09-08)

## 問題点

Elixir は Debian Stretch から main パッケージに入ったが、バージョンが 1.3.3 (erlang は 19.1) と微妙に古い。

Pleroma が Elixir 1.4 以降を require するので、Debian や Raspbian の apt から一発でインストールできないというのが問題となる。

packages.erlang-solutions.com では各種インストールパッケージを用意してくれているが、Raspbian においては jessie しか用意されておらず
最新の Raspbian (stretch) には対応していない。

Stretch ベースの Raspbian においては asdf, exenv 等のインストールマネージャを利用するか、
自前でソースコードビルドで erlang, elixir をインストールすることになるだろう。

# ユーザー領域にインストール

/home/pi 以下に環境をインストールする方法。

Elixir も /home 以下に入るため、systemd での自動起動はちょい困難。
開発、や取り敢えず動かしてみたい人向け。

## asdf インストール

参照ページ [asdf](../asdf/asdf.md)

### apt packages

あらかじめ asdf に必要な apt パッケージをインストールしておく。

ansible で書くと以下の通り。

```yaml:ansi-pleroma-aptget.yml
- hosts: all
  user: pi
  become: true

  tasks:
    - name: install essentials
      apt:
        name: "{{ item }}"
        state: latest
      with_items:
        - build-essential
        - automake
        - autoconf
        - libreadline-dev
        - libncurses-dev
        - libssl-dev
        - libyaml-dev
        - libxslt-dev
        - libffi-dev
        - libtool
        - unixodbc-dev
        - git
```

### asdf, erlang, elixir

asdf をインストールして、erlang plugin, elixir plugin を使ってプログラミング言語環境をインストールしていく。

ansible で書くと以下の通り。

```yaml:ansi-pleroma-elixier.yml
- hosts: all
  user: pi

  tasks:
    - name: clone asdf
      git:
        repo: 'https://github.com/asdf-vm/asdf.git'
        dest: "{{ ansible_env.HOME }}/.asdf"
        version: refs/tags/v0.4.0

    - name: add asdf env
      lineinfile:
        dest: "{{ ansible_env.HOME }}/.bashrc"
        line: "{{ item }}"
      with_items:
        - ". $HOME/.asdf/asdf.sh"
        - ". $HOME/.asdf/completions/asdf.bash"

    - lineinfile: dest="~/.bash_profile" create=yes line="source ~/.asdf/asdf.sh"

    - name: asdf install work
      command: "bash -lc '{{ item }}'"
      with_items:
        - "asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang"
        - "asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir"
        - "asdf plugin-update --all"
        - "asdf install erlang 20.1"
        - "asdf install elixir 1.5.2"
        - "asdf global erlang 20.1"
        - "asdf global elixir 1.5.2"
```

install erlang 部分のダウンロードとビルドにめちゃくちゃ時間がかかるので注意。

上記 ansible notebook だと冪等性を持っていないので最初の一回は成功しますが、二回目以降は plugin-add でエラーになります。
asdf の挙動によるのであしからず。


## pleroma インストール

Elixir が実行できる環境が用意できたらあとは通常の pleroma インストール手順となる。

ansible で書くと以下の通り。

```yaml:ansi-pleroma-install.yml
- hosts: all
  user: pi

  tasks:
    - name: clone preloma
      git:
        repo: 'https://git.pleroma.social/pleroma/pleroma.git'
        dest: "{{ ansible_env.HOME }}/pleroma"
        version: develop

    - name: compile preloma
      command: "bash -lc 'cd {{ ansible_env.HOME }}/pleroma;{{ item }}'"
      with_items:
      - "mix local.hex --force"
      - "mix local.rebar --force"
      - "mix clean"
      - "mix deps.get"
      - "mix deps.compile"
      - "mix compile"
      - "mix phx.digest"
```

設定の類は別途行うこと。

## postgreSQL の準備

省略、apt でインストールするなり別マシンに用意するなり。

```
% sudo -u postgres psql
	\c pleroma_dev
	CREATE EXTENSION citext;
	\q
```
