---
title: pleroma/install
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

[preloma top へ戻る](pleroma.md)

pleroma のインストールメモ

# 記述日

- Nov.19.2017 - pleroma のインストール手順書として分割

# インストール

## 動作環境の準備

Elixir 1.4 以上がインストールされた環境を用意する。
Debian Stretch の apt だと 1.3 までなので注意、これが割と問題な所かもしれない。

## データベース

Postgres を用意。

## 画像置き場

Pleroma プロジェクトディレクトリに upload フォルダが作られてそこに置かれる。

## ソースコードからのコンパイル

### ソースコード取得

```
 $ git clone https://git.pleroma.social/pleroma/pleroma.git
```

### 関連パッケージ取得とコンパイル

pleroma ディレクトリに降りて

```
 $ mix deps.get
 $ mix compile
```

### 実行

```
 $ mix phx.server
```

ただし config の設定が別途必要なので一発では動かない。後述の設定を行ってから実行すること。


### prod 設定

```
 $ MIX_ENV=prod mix compile
 $ MIX_ENV=prod mix phx.server
```

# 設定項目

## サーバー設定

各種設定は pleroma プロジェクトディレクトリ下の config に置かれる。

`config.exs` が設定ファイルでここに書かれている値が使われるけれども、内部で `[dev|prod.exs]` が読み込まれ
そこに書かれている項目が config.exs の内容を上書きする。dev か prod かは起動時の MIX_ENV で指定。


prod の場合 `prod.exs` の最後で `prod.secret.exs` が読み込まれるのだがこのファイルは存在しない。つまり、
利用者が `prod.secret.exs` を作成し自サーバーの設定項目をここに記述し一連の config を上書き設定することになる。

### 設定項目例

使いそうな所をいくつか。

httpd 設定、host url と port を設定。url は多分画像リンクの際に使われる。
前段の hhtp proxy にまかせる場合、url の中は http proxy によって公開されるパブリックな url となる、その際 https 公開するなら scheme で指定しておく。
```
config :pleroma, Pleroma.Web.Endpoint,
  on_init: {Pleroma.Web.Endpoint, :load_from_system_env, []},
  http: [port: 4000],
  url: [host: "pleroma.mofgao.space", scheme: "https", port: 443],
  protocol: "http",
  cache_static_manifest: "priv/static/cache_manifest.json"
```

PostgreSQL の設定。hostname とかは環境に合わせて設定。
```
config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "pleroma_dev",
  hostname: "localhost",
  pool_size: 10
```

インスタンス、API 設定。email は環境に合わせて設定。
```
config :pleroma, :instance,
  name: "Pleroma",
  email: "admin@example.com",
  limit: 5000,
  registrations_open: true
```


## ページ設定

ページアセットは pleroma プロジェクトディレクトリ下の `priv/` に置かれている。

変更すべきページ設定は `priv/static/static/config.json`。
サイト名と背景画像、アイコン画像が指定できる。

config.json の registration を false にすると登録不可のクローズサイトにできそう。
(おそらく config の instance 設定にある registrations_open も閉じるのだと思う)

`priv/static/static/term-of-service.html` にサイトの利用規約を記述する。
アカウント登録時に表示される文言。



## PostgreSQL

docker の postgres:alpine を使って postgres アカウントで利用するのが一番簡単。
権限的にも全権限があるので特にはまることは無い。

postgres と pleroma の conf が用意できたら mix 経由で DB を作成する。

```
 $ mix ecto.create
```

初回だと必要は無いけれども、ちゃんと DB 操作できているか確認の意味で migrate を一回通しておく。

```
 $ mix ecto.migrate
```

docker ではなく postgres を自前で立てて操作する際、migrate 操作時に citext の権限がありませんと表示されて止まる事がある。
文字通り citext の権限が無いので、全権限を持つ postgres ユーザーで psql を使って権限を付与する。

```
\c pleroma_dev
CREATE EXTENSION citext;
```

### ローカルの PostgreSQL でユーザーを作成する手順メモ

ちゃんと記録していなかったので記述ミスがあるかも

- sudo su - postgres
- createuser -d -U postgres -P pleroma
- psql
  - \c pleroma_dev
  - CREATE EXTENSION citext;
