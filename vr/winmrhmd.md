---
title: VR/WinMR_HMD
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# WinMR HMD

Windows 10 用の周辺機器である Windows Mixed Reality ヘッドセットを入手したのでそれを使うためのあれこれメモ。

主に自分で VR コンテンツを作成するための手順(予定)。

# 記述日

- Dec.24.2017 - メモページ作成

# 前提

- Windows Mixed Reality ヘッドセット + コントローラー
  - FUJITSU FMVHDS1
- Windows 10 Fall Creator Update 1709


# WinMR HMD で動作するアプリケーション

WinMR HMD は通常のデスクトップをミラー表示できるわけではなく、
専用アプリである「Mixed Reality ポータル」を起動してその中で UWP アプリを起動したり
VR 専用アプリを起動したりして過ごす。

「Mixed Reality ポータル」ではデスクトップも投映して操作できるため、UWP アプリじゃなくてもそこそこ操作可能。

## WinMR 専用の VR アプリケーション

基本的に「Mixed Reality ポータル」で起動できるものに限るので、Microsoft Store で購入した UWP アプリとなる。

WinMR HMD を駆動、コントロールする低レイヤライブラリは Mictosoft が提供する MRTK を使う。

[https://github.com/Microsoft/MixedRealityToolkit](https://github.com/Microsoft/MixedRealityToolkit)

実際のところ MRTK を直接触ることはほとんどなくて、この上に構築されている MRTK-Unity を使う方が一般的。

[https://github.com/Microsoft/MixedRealityToolkit-Unity](https://github.com/Microsoft/MixedRealityToolkit-Unity)

これらの状況から WinMR HMD 用の VR アプリを作成する通常ルートは以下のようになる。

- Unity で 3D アプリを作って UWP アプリとしてビルドする、その際 VR モードスイッチを有効にする

Unity はインストールする際に Windows をターゲットとし .NET/IL2CPP を選択して入れておく。

Unity のビルドターゲットは UWP を選択し、Player Setting の XR setting の VR サポートを設定する。
Scripting Background が IL2CPP になっていたら .NET に変更する。


## HTC Vive 用の VR アプリケーション

HTC Vive 用のアプリケーションは SteamVR 用アプリということであり、Steam で購入した HTC Vive 用アプリだけでなく
OpenVR を利用して起動する VR アプリケーションもこれに含まれる。

Steam で "Windows Mixed Reality for SteamVR" をインストールすると SteamVR で WinMR HMD が利用できる。
このことは HTC Vive 用のアプリケーションのほとんどが WinMR HMD で動作することを意味する。Steam 様々である。

HTC Vive 用のアプリケーションを起動すると SteamVR がキックされ(実質Steamの起動) SteamVR から
"Windows Mixed Reality for SteamVR" 経由で WinMR HMD が起動される。ちょい複雑。
なので WinMR は既に起動していた方が良く、WinMR HMD と Mixed Reality ポータルを既に立ち上げた状態で
HTC Vive 用アプリケーションを起動する様な形となる。

WinMR コントローラーは Vive コントローラーの代わりとしてほぼ同等に動作するが、アプリケーションによっては
タッチパット部分の十字入力が動作しないモノがある。なので全ての HTC Vive アプリケーションが楽しめるという
わけでもないので、事前確認無しに購入するのは危険である。
デモ版があったらそれを試した上でアプリケーションの購入をお勧めする。

OpenVR 自体が Vive 用の API であり、これを用いて VR アプリケーションを作成することとなる。
HTC Vive の開発環境は既にそろっているのでそれらを使えば良い。

HTC Vive 用のアプリを作成すればそのまま WinMR HMD アプリとして動作するので一番近道に見えるのだけれども
Steam の "Windows Mixed Reality for SteamVR" をインストールする必要があることとそれの出来次第というところがあり
起動手順も若干複雑なため利便性は UWP アプリケーションには劣る。


## WebVR アプリケーション

HTML5 で VR コンテンツを作成する事ができる WebVR も WinMR HMD で利用可能。

ひとつのコンテンツを作成すればどの環境でも(ブラウザ次第で)動作するし、スマホVRでも閲覧できるので閲覧側の幅が広い。

Sketchfab での HMD モードが有名。Sketchfab でモデル閲覧時に眼鏡ボタンを押すと VR 表示に移動する。

### WebVR 閲覧環境 for PC

- Edge
  - Mixed Reality ポータル内の Edge で閲覧することで利用可能、WinMR HMD での標準アクセス手順
    - Mixed Reality ポータル内から直接 VRコンテンツに移動できるので最善の VR 体験となる
- Chrome
  - OpenVR を経由して Vive モードで閲覧可能
- Firefox
  - 最近は OpenVR がインストールされていたらそれを利用できるようになった
  - OpenVR にアクセスできないと普通の全画面表示になるっぽい
  - HMD が接続されていないと SteamVR 部分でエラーになるまで停止するのがいけてない

### WebVR 閲覧環境 for スマートフォンブラウザ

- Android
  - Chrome
    - Google cardboard か Google Daydream がインストールされていたらそれがキックされ VR 表示モードになる
    - 無い場合はブラウザ上で 2眼表示になる
  - Firefox
    - 通常の 2眼表示になる
- iOS
  - 通常の 2眼表示になる
    
### WebVR 開発環境

リサーチ中

- OpenCanvas
- ReactVR
- A-Frame



# WinMR HMD で動作しないアプリケーション

- Oculus Rift 用アプリケーション
  - Oculus SDK で作られたものは動作しない
  - Oculus も Open な SDK に舵を切るらしいが？
- PSVR 用アプリケーション
  - 当然ですな


