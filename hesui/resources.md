---
title: へすいちゃんリソース
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# へすいちゃん

rerofumi さんちのアバターおよび関連コンテンツ

# 記述日

-   Oct.20.2019 - VRChat ワールドに「VR しぐさ」を追加、スライド共有の OneDrive URL を追加
-   Feb.24.2019 - モデルアップロード先に THE SEED ONLINE を追加
-   Dec.24.2018 - ページ作成

# 各種リソースリンク

-   オリジナルピクチャー (2015 年)

    -   [https://www.pixiv.net/member_illust.php?mode=medium&illust_id=53659776](https://www.pixiv.net/member_illust.php?mode=medium&illust_id=53659776)
    -   90 年代に自分のホームページに良く描いていたキャラをリメイクしたもの

-   FaceRig アバター (2016 年)

    -   [https://steamcommunity.com/sharedfiles/filedetails/?id=598318938](https://steamcommunity.com/sharedfiles/filedetails/?id=598318938)

-   VRM アバター (2018 年)

    -   [https://3d.nicovideo.jp/works/td33907](https://3d.nicovideo.jp/works/td33907)
        -   通常、どなたでも利用可能、VRM ダウンロードあり
    -   [https://3d.nicovideo.jp/works/td37579](https://3d.nicovideo.jp/works/td37579)
        -   夏服、どなたでも利用可能、VRM ダウンロードあり
    -   [https://3d.nicovideo.jp/works/td37852](https://3d.nicovideo.jp/works/td37852)
        -   体操服、どなたでも利用可能、VRM ダウンロードあり

-   ニコニコミュニティ (2018 年)

    -   [https://com.nicovideo.jp/community/co3804111](https://com.nicovideo.jp/community/co3804111)
        -   『へすいの VR 部屋』
        -   ニコ生で配信するときのお部屋

-   PeerTube (2018 年)

    -   [https://peertube.mofgao.space/videos/recently-added](https://peertube.mofgao.space/videos/recently-added)
    -   自分所の動画置き場、いくつかアーカイブ動画がここにもあったりする

-   BOOTH (2018 年)

    -   [https://fumi2kick.booth.pm/](https://fumi2kick.booth.pm/)
        -   3D モデル = 改変再配布許可付きの FBX モデルと素体モデル
        -   オンデマンドグッズ
        -   ブレスノイズキャンセルゲートの VST プラグイン (無料)

-   GLB 背景 (2018 年)

    -   [https://3d.nicovideo.jp/works/td46688](https://3d.nicovideo.jp/works/td46688)
        -   『背景/へすいの部屋』
        -   バーチャルキャストで配信するとき用のへすいさん背景モデル

-   YouTube チャンネル (2018 年)

    -   [https://www.youtube.com/channel/UC20P1XLIeQGIGx8Oq7W6FuA](https://www.youtube.com/channel/UC20P1XLIeQGIGx8Oq7W6FuA)

-   VRoid Hub (2018 年)

    -   [https://hub.vroid.com/characters/787040972615260019/models/2316542128997374899](https://hub.vroid.com/characters/787040972615260019/models/2316542128997374899)
    -   VRM モデルのアップロードテスト

-   VRChat (2018 年 ～)

    -   [https://vrchat.net/launch?worldId=wrld_ede4c8f7-aba7-42ff-bc83-533f0601df9c](https://vrchat.net/launch?worldId=wrld_ede4c8f7-aba7-42ff-bc83-533f0601df9c)

        -   "Hesui's room" (Community Labs)
        -   自分用の Home World として使う為の軽量なワールド、へすいちゃんアバターのペデストリアルが置いてあるので VRChat 内でへすいちゃんアバターが使える様になります

    -   [https://www.vrchat.com/home/launch?worldId=wrld_b0ee90fd-c36c-4da3-9061-dab36b24f137](https://www.vrchat.com/home/launch?worldId=wrld_b0ee90fd-c36c-4da3-9061-dab36b24f137)
        -   "VR しぐさ/ VR Gesture" (Community Labs)
        -   VR しているときのあるあるアニメーションを閲覧できるワールド

-   ましゅまろ (2018 年)

    -   [https://marshmallow-qa.com/hesui_channel?utm_medium=twitter&utm_source=promotion](https://marshmallow-qa.com/hesui_channel?utm_medium=twitter&utm_source=promotion)
    -   チャンネルつくったらこれもかなと思って作っておいた、質問でコミュニケーション

-   THE SEED ONLINE (2019 年～)

    -   [https://seed.online/items/477ecc511609633f05dc74edf31e6c04ee173d33fcb11cfa44b4852621ca1c7d](https://seed.online/items/477ecc511609633f05dc74edf31e6c04ee173d33fcb11cfa44b4852621ca1c7d)
        -   通常、どなたでも利用可能
    -   [https://seed.online/items/76c482eae1432c93c1fd4f787a564a111a3d72f364c10c64f92d226b63a0df69](https://seed.online/items/76c482eae1432c93c1fd4f787a564a111a3d72f364c10c64f92d226b63a0df69)
        -   夏服、どなたでも利用可能
    -   [https://seed.online/items/f5dce1f2c7571dbc650ab6c91591d724acbf261ffe1e02d9bf118ec87e1efa52](https://seed.online/items/f5dce1f2c7571dbc650ab6c91591d724acbf261ffe1e02d9bf118ec87e1efa52)
        -   体操服、どなたでも利用可能
    -   [https://seed.online/items/7d313702a78b2c8b1a3ae3a4c589eaa1af3e1f7c3348e927950213abfc0b2a41](https://seed.online/items/7d313702a78b2c8b1a3ae3a4c589eaa1af3e1f7c3348e927950213abfc0b2a41)
        -   へすいの部屋、ワールド glb モデル
    -   [https://seed.online/items/67db9b28fd1d2a7c318bc64ad4bba98a8b2db4f0a0e89730715d3cc66826eb79](https://seed.online/items/67db9b28fd1d2a7c318bc64ad4bba98a8b2db4f0a0e89730715d3cc66826eb79)
        -   CG 部屋、ワールド glb モデル

-   スライド (2017 年～)

    -   これまでスライドはニコナレに上げていたが、ニコナレが 2019 年 8 月で終了してしまったので参照先が無くなった
    -   スライドの共有方法として OneDrive を使ってみることにします
        -   [https://1drv.ms/f/s!AlYmvE7QWYCxpu5-lo-14DJVNckQow](https://1drv.ms/f/s!AlYmvE7QWYCxpu5-lo-14DJVNckQow)
