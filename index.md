---
title: toppage
sitename: Fumi2kick ScrapBook
url: http://fmscb.fumi2kick.com/
exlink: https://bitbucket.org/rerofumi/fm-scrapbook/issues
exmessage: 指摘事項がありましたら issue としてご登録ください
---

# TOP PAGE

ソフトウェアやプログラミングについての書き散らかしメモ。

## もくじ

[asdf](asdf/asdf.md)

[pleroma](pleroma/pleroma.md)

[mastodon](mastodon/mastodon.md)

[WinMR HMD VR メモ](vr/winmrhmd.md)

[PeerTube](peertube/peertube.md)

[Kubernetes](kubernetes/kubernetes.md)

[へすいちゃんリソース](hesui/resources.md)
